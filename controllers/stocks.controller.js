const { Verify } = require('crypto');
const express = require('express');
const mongoose = require('mongoose');
const User = mongoose.model('User');
const Stock = mongoose.model('stock');
const Commande = mongoose.model('Commande');

module.exports.createStock = async (req, res) => {

    try {

        if (req.body.nomProduit == '') {
            res.jsonp({
                msg: 'nom required',
                statut: false
            })

        } else if (req.body.codeProduit == '') {
            res.jsonp({
                msg: 'code required',
                statut: false
            })

        } else if (req.body.categorieProduit == '') {

            res.jsonp({
                msg: 'categorie required',
                statut: false
            })

        } else if (req.body.prixUnite == '') {

            res.jsonp({
                msg: 'prix required',
                statut: false
            })

        } else if (req.body.quantiteProduit == '') {

            res.jsonp({
                msg: 'quantite required',
                statut: false
            })

        }  else {

            var ifStock = await Stock.findOne({ 'nom': req.body.nomProduit })
           
            if (!ifStock){

                var stock = new Stock({
                    nom: req.body.nomProduit,
                    categorie: req.body.categorieProduit,
                    code: req.body.codeProduit,
                    quantite: req.body.quantiteProduit,
                    prix: req.body.prixUnite,
                    description: req.body.description
                   

                });

                let data = await stock.save();
                if (data) {
                    res.jsonp({
                        msg: 'le stock a été enregistré avec success',
                        statut: true,
                        stock: data
                    })

                } 

            }else {
                res.jsonp({
                    msg: 'Le produit existe déja',
                    statut: false
                })
            }
        }
    }catch (e) {
        console.error(e);

    }
}

module.exports.getSockByCategorie = async (req, res) => {


        try {
            Stock.find({ 'categorie': req.params.categorie })
                .then(produit => {
                    if (produit) {

                        res.jsonp({
                            produit: produit,
                            statut: true
                        })
                    } else {
                        res.jsonp({
                            msg: 'le produit n\'existe pas',
                            statut: false
                        })
                    }
                })

        }
        catch (e) {
            console.error(e);

        }
    }
      

    module.exports.getSockByNom = async (req, res) => {


        try {
            Stock.find({ 'nom': req.params.nom })
                .then(produit => {
                    if (produit) {

                        res.jsonp({
                            produit: produit,
                            statut: true
                        })
                    } else {
                        res.jsonp({
                            msg: 'le produit n\'existe pas',
                            statut: false
                        })
                    }
                })

        }
        catch (e) {
            console.error(e);

        }
    }

module.exports.getProduct = async (req, res) => {

        Stock.find((err, docs) => {
            if (!err) {

                res.json({ product: docs })
            }
            else {
                res.send('Erreur dans l affichage des Products:' + err);
            }
        });
 }


module.exports.commande = async (req, res, next) => {

        try {

            if (req.body.nom == '') {
                res.jsonp({
                    msg: 'nom required',
                    statut: false
                })

            } else if (req.body.quantite == '') {
                res.jsonp({
                    msg: 'categorie required',
                    statut: false
                })


            }else {

                var ifStock = await Stock.findOne({ 'nom': req.body.nom })
                console.log(ifStock)
                if(ifStock){
                if (ifStock.quantite >= req.body.quantite) {

                    var newQuantite = ifStock.quantite - req.body.quantite
                    var vendu = req.body.quantite + ifStock.vendu
                    var updateQuantite = await Stock.findOneAndUpdate({ _id: ifStock._id }, { quantite: newQuantite })
                    if (updateQuantite) {

                        var commande = new Commande({
                            nom: req.body.nom,
                            quantite: req.body.quantite,
                            code: ifStock.code,
                            couleur: req.body.couleur,
                            prixttc: req.body.quantite * ifStock.prix,
                            acheteur: req.body.acheteur,
                            adresse: req.body.adresse,
                            contact: req.body.contact
                        })
                        let resCommande = await commande.save();
                        var updateVendu = await Stock.findOneAndUpdate({ _id: ifStock._id }, { vendu: vendu })
                        if (updateVendu) {

                            res.jsonp({
                                msg: 'commande validé',
                                statut: true
                            })
                           
                        }

                    } else {
                        res.jsonp({
                            msg: 'impossible d\'effectuer une commande',
                            statut: false
                        })
                    }



                } else {
                    res.jsonp({
                        msg: 'la quantité du produit ne satifait est inferieure a la demande',
                        statut: false
                    })
                }
            } else {
                res.jsonp({
                    msg: 'produit inexistant',
                    statut: false
                })
            }

            }
        }
        catch (e) {
            console.error(e);

        }
    }

module.exports.printerCommande = async (req, res, next) => {

        try {

            if (req.body.nom == '') {
                res.jsonp({
                    msg: 'nom required',
                    statut: false
                })

            } else if (req.body.categorie == '') {
                res.jsonp({
                    msg: 'categorie required',
                    statut: false
                })

            } else if (req.body.code == '') {

                res.jsonp({
                    msg: 'code required',
                    statut: false
                })

            } else if (req.body.quantite == '') {

                res.jsonp({
                    msg: 'quantite required',
                    statut: false
                })

            } else {

                const ifStock = await Stock.findOne({ 'nom': req.body.nom })
                if (!ifStock) {

                    var stock = new Stock({
                        nom: req.body.nom,
                        categorie: req.body.categorie,
                        code: req.body.code,
                        quantite: req.body.quantite



                    });

                    let data = await stock.save();
                    if (data) {
                        res.jsonp({
                            msg: 'Utilisateur enregistré avec success',
                            statut: true,
                            stock: data
                        })

                    } else {

                        res.jsonp({
                            msg: 'Enregistrement echoué',
                            statut: false
                        })

                    }

                } else {
                    res.jsonp({
                        msg: 'existe deja',
                        statut: false
                    })
                }

            }
        }
        catch (e) {
            console.error(e);

        }
    }

module.exports.deleteByID = async (req, res, next) => {

    Stock.findByIdAndRemove(req.params.id, (err, doc) => {
        if (!err) {
            res.jsonp({ msg: 'User delete',
                        statut: true })
        } else { res.jsonp({ msg: 'error',
                              statut: false }) }
    });


          
        
    }

