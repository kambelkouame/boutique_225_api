//import
//require('./config/config');
require('./db/db.config');
//dependance

const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const passport = require('passport');
const path = require('path');
const morgan = require('morgan');

//routers Imports
var core_config  = require('./router/api.connect.router');
var core_user  = require('./router/user.router');
var core_stock  = require('./router/stock.router');

const whitelist = ['http://localhost:4200', 'http://127.0.0.1:4200'];
const corsOptions = {
    origin: function (origin, callback) {
        return callback(null, true); // allow all
          
        if (whitelist.indexOf(origin) !== -1) {
          return callback(null, true)
        } else {
          return callback(new Error('Not allowed by CORS'))
        }
    },
}
var app = express();

// morgan
app.use(morgan('dev'));

// middleware
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cors(corsOptions));
app.use(passport.initialize());

app.set('view engine', 'pug');
app.set("views", path.join(__dirname, 'public/views'));


app.use('/core', core_config);
app.use('/core/user/', core_user);
app.use('/core/stock/', core_stock);

app.listen(process.env.PORT||3000, () => console.log(`Module-core started at port`));
