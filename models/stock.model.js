const mongoose = require('mongoose');

var stockSchema = new mongoose.Schema({
    nom: {
        type: String, 
    },
    categorie: {
        type: String,

    },
    code: {
        type: String
    },
    quantite: {
        type: Number,
    },
    prix: {
        type: Number,
    },
    description: {
        type: String,
    },

    vendu: {
        type: Number,
        default:0
    },
    CreationDate: {
        type: Date,
        default: Date.now()
    }

    
});

mongoose.model('stock', stockSchema);