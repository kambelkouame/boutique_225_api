const mongoose = require('mongoose');

var userSchema = new mongoose.Schema({
    nom: {
        type: String, 
    },
    prenom: {
        type: String,

    },
    telephone: {
        type: String,

    },
    password: {
        type: String
    },
    statut: {
        type: Boolean,
        default:true
    },
    CreationDate: {
        type: Date,
        default: Date.now()
    },
    lastConnect: {
        type: Date
    }

    
});

mongoose.model('User', userSchema);