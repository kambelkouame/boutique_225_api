const mongoose = require('mongoose');

var commandeSchema = new mongoose.Schema({
 
    nom: {
        type: String
    },
    quantite: {
        type: Number
       
    },
    code: {
        type: String
       
    },
    couleur: {
        type: String
       
    },
    prixttc: {
        type: Number
       
    },
    acheteur: {
        type: String
       
    },
    adresse: {
        type: String,
        default:0
    },
    contact: {
        type: String,
        default:0
    },
    CreationDate: {
        type: Date,
        default: Date.now()
    }

    
});

mongoose.model('Commande', commandeSchema);