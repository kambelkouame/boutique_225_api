const express = require('express');
const router = express.Router();
const path = require('path');


var  stock_controller = require('../controllers/stocks.controller');


router.post('/newStock', stock_controller.createStock);
router.get('/getAllproduct', stock_controller.getProduct);
router.post('/commande', stock_controller.commande);
router.get('/getSockByNom/:nom', stock_controller.getSockByNom);
router.get('/deleteStock/:id', stock_controller.deleteByID);
module.exports = router;